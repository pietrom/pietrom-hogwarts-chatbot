FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln ./
WORKDIR HogwartsBot
COPY HogwartsBot/*.csproj .
WORKDIR /app
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out HogwartsBot/HogwartsBot.csproj

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
COPY --from=build-env /app/HogwartsBot/out .
ENTRYPOINT ["dotnet", "HogwartsBot.dll"]