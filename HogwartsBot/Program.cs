﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Npgsql;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace HogwartsBot {
    class Program {
        static ITelegramBotClient botClient;
        private static IDbConnection conn;

        static void Main() {
            var envVars = Environment.GetEnvironmentVariables();
            foreach (var envVar in envVars.Keys) {
                Console.WriteLine($"{envVar} --> {envVars[envVar]}");
            }

            var connectionString = GetConnectionString();
            conn = new NpgsqlConnection(connectionString);
            conn.Open();

            var token = Environment.GetEnvironmentVariable("TELEGRAM_BOT_TOKEN");
            Console.WriteLine($"Connecting to Telegram... [{token.Substring(0,5)}]");
            botClient = new TelegramBotClient(token);
            try {
                var me = botClient.GetMeAsync().Result;
                Console.WriteLine(
                    $"Hello, World! I am user {me.Id} and my name is {me.FirstName}."
                );

                botClient.OnMessage += Bot_OnMessage;
                botClient.StartReceiving();
                Thread.Sleep(int.MaxValue);
            }
            catch (Exception e) {
                Console.WriteLine(e.Message, e.StackTrace);
            }
        }

        private static string GetConnectionString() {
            var dbUsername = Environment.GetEnvironmentVariable("DB_USERNAME");
            var dbPassword = Environment.GetEnvironmentVariable("DB_PASSWORD");
            var dbHost = Environment.GetEnvironmentVariable("DB_HOST");
            var dbPort = Environment.GetEnvironmentVariable("DB_PORT");
            var dbName = Environment.GetEnvironmentVariable("DB_NAME");
            return $"Server={dbHost};Port={dbPort};User Id={dbUsername};Password={dbPassword};Database={dbName};";
        }

        static async void Bot_OnMessage(object sender, MessageEventArgs e) {
            try {
                if (e.Message.Text != null) {
                    bool sent = false;
                    Console.WriteLine($"Received a text message in chat {e.Message.Chat.Id}.");
                    int? cid = null;
                    string lastName = null;
                    using (IDbCommand cmd = conn.CreateCommand()) {
                        cmd.CommandText = "select id, first_name, last_name, image_url from Character where first_name = @firstName";
                        IDbDataParameter parameter = cmd.CreateParameter();
                        parameter.ParameterName = $"@firstName";
                        parameter.Value = e.Message.Text;
                        cmd.Parameters.Add(parameter);
                        using (IDataReader reader = cmd.ExecuteReader()) {
                            if (reader.Read()) {
                                lastName = reader.GetString(2);
                                await botClient.SendTextMessageAsync(
                                    chatId: e.Message.Chat,
                                    text: $"Found {reader.GetString(1)} {lastName}"
                                );

                                cid = reader.GetInt32(0);
                                reader.Close();
                            }
                        }
                    }

                    if (cid.HasValue) {
                        using (IDbCommand cmd2 = conn.CreateCommand()) {
                            cmd2.CommandText = "select url from character_picture where character_id = @cid";
                            IDbDataParameter parameter2 = cmd2.CreateParameter();
                            parameter2.ParameterName = $"@cid";
                            parameter2.Value = cid.Value;
                            cmd2.Parameters.Add(parameter2);

                            var list = new List<string>();
                            using (IDataReader reader2 = cmd2.ExecuteReader()) {
                                while (reader2.Read()) {
                                    list.Add(reader2.GetString(0));
                                }
                            }

                            if (list.Any()) {
                                await botClient.SendPhotoAsync(
                                    chatId: e.Message.Chat,
                                    photo: list[new Random().Next(list.Count)],
                                    caption: $"<b>{e.Message.Text} {lastName}</b>.",
                                    parseMode: ParseMode.Html
                                );
                            }
                        }
                    }
                    else {
                        await botClient.SendTextMessageAsync(
                            chatId: e.Message.Chat,
                            text:   CreateResponse(e.Message)
                        );

                        if (Characters.ContainsKey(e.Message.Text)) {
                            Message message = await botClient.SendPhotoAsync(
                                chatId: e.Message.Chat,
                                photo: $"https://gitlab.com/pietrom/hello-hogwarts-chatbot-resources/-/raw/master/{e.Message.Text}.jpg",
                                caption: $"<b>{e.Message.Text} {Characters[e.Message.Text]}</b>.",
                                parseMode: ParseMode.Html
                            );
                        }
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message, ex);
            }
        }

        private static string CreateResponse(Message msg) {
            if (Characters.ContainsKey(msg.Text)) {
                return $"Hello, {msg.Text} {Characters[msg.Text]}";
            }

            return $"Hello, {msg.Text}! Known characters are {string.Join(',', Characters.Keys)}";
        }

        static readonly IDictionary<string, string> Characters = new Dictionary<string, string> {
            {"Harry", "Potter"},
            {"Hermione", "Granger"},
            {"Ron", "Weasley"},
            {"Albus", "Silente"},
            {"Severus", "Piton"},
            {"Draco", "Malfoi"},
            {"Neville", "Paciock"},
            {"Luna", "Lovegood"},
            {"Jini", "Weasley"},
            {"Lucius", "Malfoi"},
            {"Tom", "Riddle"},
            {"Bellatrix", "Lestrange"},
            {"Voldemort", ""}
        };
    }
}